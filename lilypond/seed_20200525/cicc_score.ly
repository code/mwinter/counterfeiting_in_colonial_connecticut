\version "2.19.83"

\include "cicc_pseudoindents_def.ly"

#(define factor 2)

#(define (enlarged-extent-laissez-vibrer::print grob)
  (let* ((stil (laissez-vibrer::print grob))
         (stil-ext (ly:stencil-extent stil X))
         (stil-length (interval-length stil-ext))
         (new-stil-length (* stil-length factor))
         (scale-factor (/ new-stil-length stil-length))
         (new-stil (ly:stencil-scale stil scale-factor 1))
         (new-stil-ext (ly:stencil-extent new-stil X))
         (x-corr (- (car stil-ext) (car new-stil-ext))))
  (ly:stencil-translate-axis
     new-stil
     x-corr
     X)))

#(assoc-set! (assoc-ref all-grob-descriptions 'LaissezVibrerTie)
'stencil enlarged-extent-laissez-vibrer::print)

\paper {
  #(set-paper-size "a4" 'portrait)
  top-margin = 1 \cm
  bottom-margin = 1 \cm
  left-margin = 2.25 \cm
  ragged-bottom = ##t
  
  top-system-spacing =
  #'((basic-distance . 20 )
  (minimum-distance . 20 )
  (padding . 0 )
  (stretchability . 0))
  
  system-system-spacing = 
  #'((basic-distance . 25 )
  (minimum-distance . 25 )
  (padding . 0 )
  (stretchability . 0))
  
  last-bottom-spacing =
  #'((basic-distance . 15 )
  (minimum-distance . 15 )
  (padding . 0 )
  (stretchability . 0))
  
  systems-per-page = 5
  first-page-number = 6
  print-first-page-number = ##t
  
  print-page-number = ##t
  oddHeaderMarkup = \markup { \fill-line { \line { \on-the-fly #not-first-page {\italic {Counterfeiting in Colonial Connecticut} (seed: 20200525)}}}}
  evenHeaderMarkup = \markup { \fill-line { \line { \on-the-fly #not-first-page {\italic {Counterfeiting in Colonial Connecticut} (seed: 20200525)}}}}
  oddFooterMarkup = \markup { \fill-line {
    \concat {
      "-"
      \fontsize #1.5
      \on-the-fly #print-page-number-check-first
      \fromproperty #'page:page-number-string
      "-"}}}
  evenFooterMarkup = \markup { \fill-line {
    \concat { 
      "-" 
      \fontsize #1.5
      \on-the-fly #print-page-number-check-first
      \fromproperty #'page:page-number-string
      "-"}}}
}

\header {
  title = \markup { \italic {Counterfeiting in Colonial Connecticut}}
  composer = \markup \right-column {"michael winter" "(cdmx and gatlinburg, tennessee; 2020)"}
  poet = "seed: 20200525"
  tagline = ""
} 

#(set-global-staff-size 11)

\layout {
  indent = 0.0\cm
  line-width = 17\cm 
  ragged-last = ##f
  ragged-right = ##f
  
  \context {
    \Score
      \override BarNumber.stencil = #(make-stencil-circler 0.1 0.25 ly:text-interface::print)
      \override Stem.stemlet-length = #0.75
      proportionalNotationDuration = #(ly:make-moment 1/16)
      \remove "Separating_line_group_engraver"
  }
  \context {
    \Staff
    
    \override VerticalAxisGroup.staff-staff-spacing =
      #'((basic-distance . 15 )
      (minimum-distance . 15 )
      (padding . 0 )
      (stretchability . 0))
      
     \override RehearsalMark.X-offset = #1
      \override RehearsalMark.Y-offset = #4
      \override VerticalAxisGroup.default-staff-staff-spacing =
      #'((basic-distance . 16 )
      (minimum-distance . 16 )
      (padding . 0 )
      (stretchability . 0))
      
      \override TimeSignature.font-size = #2
      \override TimeSignature.break-align-symbol = #'clef
      \override TimeSignature.X-offset =
        #ly:self-alignment-interface::x-aligned-on-self
      \override TimeSignature.self-alignment-X = #LEFT
      \override TimeSignature.Y-offset = #9
      \override TimeSignature.extra-offset = #'(2 . 0)
      \override TimeSignature.break-visibility = #end-of-line-invisible
  }
  \context {
    \StaffGroup
    \name "SemiStaffGroup"
    \consists "Span_bar_engraver"
    \override SpanBar.stencil =
      #(lambda (grob) 
        (if (string=? (ly:grob-property grob 'glyph-name) "|")
            (set! (ly:grob-property grob 'glyph-name) ""))
        (ly:span-bar::print grob))
  }
  \context {
    \Score
    \accepts SemiStaffGroup
  }
}

\score{
\new Score 
  <<
    \new SemiStaffGroup {
    <<
    \new Staff \with {
      instrumentName = "high"
      shortInstrumentName = "high"
    }
    <<
      \include "includes/cicc_high.ly"
    >>

    \new Staff \with {
      instrumentName = "guitar"
      shortInstrumentName = "guitar"
    }
    <<
      \include "includes/cicc_guitar.ly"
    >>
    
    \new Staff \with {
      instrumentName = "low"
      shortInstrumentName = "low"
    }
    <<
      \include "includes/cicc_low.ly"
    >>
    
    >>
    }
  >>
  
  \layout{}
}
